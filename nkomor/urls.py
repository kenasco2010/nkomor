"""nkomor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.authtoken import views
# for web login
from django.contrib.auth import views as auth_views

from nkomor import settings
from users.views import UserViewSet
from users.views import password_reset, signup, done_signup
from landing_page.views import landing_page
from company_profile.views import *
from user_profile.views import *
router = DefaultRouter()

router.register('v1/users', UserViewSet)
router.register('v1/company-profiles', CompanyProfileViewSet)
router.register('v1/user-profiles', UserProfileViewSet)

urlpatterns = [
    # url for rest framework
    url(r'^api/', include(router.urls)),

    url(r'^admin/', include(admin.site.urls)),

    # web view login
    url(r'^accounts/login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^accounts/password_reset/$', auth_views.password_reset,
            {'template_name': 'registration/customized_password_reset_form.html',
            'html_email_template_name': 'registration/customized_password_reset_email.html',
            'subject_template_name': 'registration/customized_password_reset_subject.txt'},
            name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm),
    url(r'^accounts/reset/done/$', auth_views.password_reset_complete,
            {'template_name': 'registration/customized_password_reset_complete.html'},
            name='password_reset_complete'),
    # ---------------
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^accounts/register/$', signup, name='signup'),

    # landing page urls.
    url(r'^welcome', landing_page, name='landing-page'),
    url(r'^accounts/registeration/done/$', done_signup, name='done-signup'),

    # url for company profie
    url(r'^add-company-profile', add_company_profile, name="add-company-profile"),
    url(r'^view-company-profile', view_company_profile, name="view-company-profile"),
    url(r'^company-profile/(?P<pk>\d+)/edit$', edit_company_profile, name="edit-company-profile"),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
