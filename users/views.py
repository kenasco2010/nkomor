# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect

import logging
from django.contrib.auth.views import *
import re
from rest_framework import viewsets
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import status
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate

from users.models import User
from users.serializers import UserSerializer, \
                              UserLoginSerializer
from users.permissions import IsOwner
from users.forms import SignupForm
from django.template.loader import get_template
from django.template import Context
from django.views.generic import CreateView


logger = logging.getLogger('nkomor')


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (IsOwner,)
    serializer_class = UserSerializer
    queryset = User.objects.all()

    # list users but only logged in user's data will show because of (IsOwner)
    def list(self, request, *arg, **kwargs):
        users = [request.user]
        users = self.serializer_class(users, many=True)
        return Response({"status_code": status.HTTP_200_OK,
                         "message": "list of logged in users",
                         "results":  users.data})

    # view to creat user, anyone can access this view
    @list_route(methods=('POST',),  permission_classes=(AllowAny,))
    def signup(self, request, *args, **kwargs):
        """
        Allows a user to signup to the platform using email
        POST PARAMETERS:
        email = Email of User
        password = Password of User
        """
        email = request.data.get('email', None)
        # username = request.data.get('username', None)
        password = request.data.get('password', None)

        # checks if the input in the email field is valid with regex (re)
        validate_email = re.search(r'[\w.-]+@[\w.-]+.\w+', email)
        # check if username and email is not empty
        # if not username:
        #     return Response({'detail': 'please enter username'}, status=HTTP_401_UNAUTHORIZED)
        if not email or not validate_email:
            return Response({"status_code": status.HTTP_401_UNAUTHORIZED,
                             "message": "please enter a valid email address",
                             })

        # This checks if the username and email already exist.
        # if User.objects.filter(username=username).exists():
        #     return Response({'detail': 'Username already exist, please use different username'}, status=HTTP_401_UNAUTHORIZED)
        if User.objects.filter(email=email).exists():
            return Response({"status_code": status.HTTP_401_UNAUTHORIZED,
                            "message": "email already exist, please use different email"
                             })

        if email is not None \
            and password is not None:

            # mymodel = get_user_model()
            user = User.objects.create_user(
                email=email,
                password=password
            )

            # user.set_password(password)
            # user.save()
            # user = authenticate(username=user.username,  password=password)
            # login(request, user)
            user = self.serializer_class(user)
            return Response({"status_code": status.HTTP_201_CREATED,
                             "message": "you have successfully signed up on nkomor",
                             "result": user.data
                             })
        else:
            return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                            "message": "Please supply required parameters"
                             })


    @list_route(methods=('POST',),  permission_classes=(AllowAny,))
    def login(self, request, *args, **kwargs):
        """
        Allow users to login using username and password

        POST PARAMETERS:
        username = Typed Username
        password = Of course Password typed
        """
        email = request.data.get('email', None)
        password = request.data.get('password', None)
        user = authenticate(email=email, password=password)

        if user is not None:
            if user.is_active:
                Token.objects.get(user=user).delete()
                Token.objects.create(user=user)
                # login(request, user)
                user = UserLoginSerializer(user)
                return Response({"status_code": status.HTTP_200_OK,
                                 "message": "you have successfully logged in",
                                 "result": user.data
                                })
            else:
                return Response({"status_code": status.HTTP_404_NOT_FOUND,
                                 "message": "This account has been deactivated"
                                 })
        else:
            return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                            "message": "Invalid username or password"
                             })

    # This endpoint not in use
    @detail_route(methods=('POST',))
    def logout(self, request, pk=None, *args, **kwargs):
        """
        Logs out a user
        """
        user = self.get_object()
        try:
            Token.objects.get(user=user).delete()
            Token.objects.create(user=user)
            return Response({"status_code": status.HTTP_200_OK,
                            "message": "You have been successfully logged out"
                             })
        except:
            return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                             "message": "Sorry we could not log you out"
                             })


    # def password_reset_redirect(self, request, *args, **kwargs):


@deprecate_current_app
@csrf_protect
def password_reset(request,
                   template_name='registration/password_reset_form.html',
                   email_template_name=get_template('registration/customized_password_reset_email.html'),
                   subject_template_name=get_template('registration/password_reset_subject.txt'),
                   password_reset_form=PasswordResetForm,
                   token_generator=default_token_generator,
                   post_reset_redirect=None,
                   from_email=None,
                   extra_context=None,
                   html_email_template_name=None,
                   extra_email_context=None):
    # try:
    if post_reset_redirect is None:
        post_reset_redirect = reverse('password_reset_done')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    if request.method == "POST":
        form = password_reset_form(request.POST)
        if form.is_valid():
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                'from_email': from_email,
                'email_template_name': email_template_name,
                'subject_template_name': subject_template_name,
                'request': request,
                'html_email_template_name': html_email_template_name,
                'extra_email_context': extra_email_context,
            }
            form.save()
            return HttpResponseRedirect(post_reset_redirect)

    else:
        form = password_reset_form()
    context = {
        'form': form,
        'title': ('Password reset'),
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)

    # except Exception, e:
    #     logger.exception(e)

def done_signup(request):
    return render(request, 'registration/done_signup.html')


# dashboard signup
def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = True
            user.save()
            return redirect('done-signup')  
    else:
        form = SignupForm()
    
    return render(request, 'registration/user_signup.html', {'form': form})