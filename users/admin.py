# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from users.models import User
from django.contrib.auth.admin import UserAdmin
# Register your models here.


admin.site.register(User)
