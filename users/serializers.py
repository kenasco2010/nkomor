from rest_framework import serializers

from users.models import User
from user_profile.models import UserProfile


class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile

        fields = (
                "id","first_name", "last_name", "phone_number", "profile_image",
                )


class UserSerializer(serializers.ModelSerializer):
    auth_token = serializers.CharField(max_length=500, read_only=True)

    class Meta:
        model = User
        fields =('id','email', 'auth_token')

class UserLoginSerializer(serializers.Serializer):
    auth_token = serializers.CharField(max_length=500, read_only=True)
    email = serializers.CharField(max_length=100)
    userprofile = UserProfileSerializer()
    class Meta:
        model = User
        fields =('id','email', 'auth_token','userprofile')

