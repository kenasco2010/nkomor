from django import forms
from django.forms import TextInput, PasswordInput
from .models import User
from django.contrib.auth.forms import UserCreationForm
# from django.contrib.auth.models import User


class SignupForm(UserCreationForm):
    email = forms.CharField(
        label = "Email",
        max_length=200, 
        help_text='Required',
        widget=forms.TextInput(attrs={'class': "form-control signup"})
        )
    
    password1 = forms.CharField(
        label = "password",
        help_text='Required',
        widget=forms.PasswordInput(attrs={'class': "form-control signup"})
        )
    
    password2 = forms.CharField(
        label = "confirm password",
        help_text='Required',
        widget=forms.PasswordInput(attrs={'class': "form-control signup"})
        
        )
    class Meta:
        model = User
        fields = ('email', 'password1', 'password2')