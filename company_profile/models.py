# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill

from django.db import models
from users.models import User
from django.conf import settings
from datetime import date
from .choices import *
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')



class CompanyProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    facility_name = models.CharField(max_length=100, blank=True)
    working_hours = models.CharField(max_length=30, blank=True)
    location = models.CharField(max_length=50, blank=True)
    description = models.TextField(blank=True)
    specialization = models.TextField(blank=True)
    contact_number = models.CharField(max_length=15, blank=True)
    email = models.CharField(max_length=30, blank=True)
    website_url = models.CharField(max_length=30, blank=True)
    admin_name = models.CharField(max_length=30, blank=True)
    admin_contact = models.CharField(max_length=15, blank=True)
    md_name = models.CharField(max_length=30, blank=True)
    md_contact = models.CharField(max_length=15, blank=True)
    facility_type = models.CharField(choices=FACILITY_TYPE, max_length=20, null=True, blank=True)
    company_image = ProcessedImageField(upload_to='company-images',
                                        format='JPEG',
                                        options={'quality': 30},
                                        null=True, blank=True)

    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return self.facility_name

    def __str__(self):
        return self.facility_name

    class Meta:
        verbose_name_plural = "Company Profile"


