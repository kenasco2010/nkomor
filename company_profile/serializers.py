from rest_framework import serializers
from django.contrib.auth import get_user_model
from django.http import Http404, HttpResponseForbidden
from rest_framework.decorators import (api_view, permission_classes, list_route)
from django.core.validators import validate_email
from rest_framework.serializers import ModelSerializer

from .models import CompanyProfile


class CompanyProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanyProfile

        fields = (
                "id","facility_name", "description", "location", "specialization",
                "working_hours", "contact_number", "email", "website_url",
                "admin_name", "admin_contact", "md_name", "md_contact", "facility_type",
                "company_image"
                )
