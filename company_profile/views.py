# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import redirect, render, get_object_or_404
from django.views.generic.base import View
from django.contrib.auth.decorators import *
from django.http import HttpResponse
from django.utils import timezone

from .forms import CompanyProfileForm
from .models import CompanyProfile

# for api
import logging
from django.contrib.auth.views import *
import re
from rest_framework import viewsets
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import status
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate

from .serializers import CompanyProfileSerializer
from .permissions import IsOwner
from django.template.loader import get_template
from django.template import Context

# @login_required()
def add_company_profile(request):
    if request.method == "POST":
        company_profile_form = CompanyProfileForm(request.POST, request.FILES)
        if company_profile_form.is_valid():
            company_profile_form = company_profile_form.save(commit=False)
            company_profile_form.user = request.user
            company_profile_form.published_date = timezone.now()
            company_profile_form.save()
            return redirect('view-company-profile')
    else:
        company_profile_form = CompanyProfileForm()
    return render (request, 'company_profile/add_company_profile.html', {'form': company_profile_form})

@login_required()
def edit_company_profile(request, pk):
    comp_prof = get_object_or_404(CompanyProfile, pk=pk)
    if request.method == "POST":
        company_profile_form = CompanyProfileForm(request.POST, request.FILES, instance=comp_prof)
        if company_profile_form.is_valid():
            company_profile_form = company_profile_form.save(commit=False)
            company_profile_form.user = request.user
            company_profile_form.published_date = timezone.now()
            company_profile_form.save()
            return redirect('view-company-profile')
    else:
        company_profile_form = CompanyProfileForm(instance=comp_prof)
    return render (request, 'company_profile/edit_company_profile.html', {'form': company_profile_form})

# @login_required()
def view_company_profile(request):
    try:
        if request.user.is_authenticated():
            user = request.user
            get_company_profile = CompanyProfile.objects.get(user=user)
            # if get_company_profile.company_name:
            #   return {"company_name": get_company_profile.company_name}
            # else:
            #     return {"company_name": "/create-company-profile/"}
            context = {
            'company_profile': get_company_profile
            }
            return render(request, 'company_profile/view_company_profile.html', context)
    except CompanyProfile.DoesNotExist:
        return render(request, 'company_profile/prompt_to_add_company_profile.html')


# Rest API Endpoints.
class CompanyProfileViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (AllowAny,)
    serializer_class = CompanyProfileSerializer
    queryset = CompanyProfile.objects.all()
