from django import forms
from django.forms import TextInput
from .models import CompanyProfile
from django.contrib.auth.models import User
from django.contrib.auth.models import *

from .choices import *
class CompanyProfileForm(forms.ModelForm):
    facility_name = forms.CharField(
        label = "Facility Name",
        widget=forms.TextInput(attrs={"class": "form-control company-form", "placeholder":"Your facility name in full"}),
        )
    description = forms.CharField(
        label = "Description of facility",
        required=False,
        widget=forms.Textarea(attrs={"class": "form-control company-form", "placeholder":"Description of your facility",
                                     "rows": 4}),
        )
    location = forms.CharField(
        label = "Location",
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control company-form",
                                    "placeholder":"Location of your facility"}),
        )

    specialization = forms.CharField(
        label = "Facility specialization",
        required=False,
        widget=forms.Textarea(attrs={"class": "form-control company-form",
                                    "placeholder":"What is your facility specialized in", "rows": 4}),
        )

    working_hours = forms.CharField(
        label = "Working hours",
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control company-form",
                                    "placeholder":"Working hourse eg. 7:00am - 5:00pm"}),
        )
    contact_number = forms.CharField(
        label = "Contact number",
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control company-form",
                                "placeholder":"Provide your contact number"}),
        )
    email = forms.CharField(
        label = "Email",
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control company-form",
                                    "placeholder":"Provide email" }),
        )
    website_url = forms.CharField(
        label = "Website url",
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control company-form",
                                    "placeholder":"eg. https://www.example.com" }),
        )
    admin_name = forms.CharField(
        label = "Name of Admin",
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control company-form",
                                    "placeholder":"Provide admin name" }),
        )
    admin_contact = forms.CharField(
        label = "Admin contact",
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control company-form",
                                    "placeholder":"Admin contact" }),
        )
    md_name = forms.CharField(
        label = "Managing director name",
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control company-form",
                                    "placeholder":"Enter managing director name" }),
        )
    md_contact = forms.CharField(
        label = "Managing director contact",
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control company-form",
                                    "placeholder":"Enter managing director contact" }),
        )
    facility_type = forms.ChoiceField(
        label = "Select facility type",
        required=False,
        choices=FACILITY_TYPE,
        initial='Select a facility type',
        widget=forms.Select(attrs={"class": "form-control company-form"}),
        )
    class Meta:
        model = CompanyProfile
        fields = [
                "facility_name", "description", "location", "specialization",
                "working_hours", "contact_number", "email", "website_url",
                "admin_name", "admin_contact", "md_name", "md_contact", "facility_type",
                "company_image"
                ]
