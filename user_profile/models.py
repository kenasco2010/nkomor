# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils import timezone
from django.conf import settings
from django.db import models
from users.models import User
from datetime import date
from choices import *


AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

# Create your models here.


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=90, blank=True)
    last_name = models.CharField(max_length=90, blank=True)
    phone_number = models.CharField(max_length=20, blank=True)  # Phone number of the user

    email = models.CharField(max_length=90, blank=True)
    date_of_birth = models.DateField(("Date"), default=date.today, null=True, blank=True)
    location = models.CharField(max_length=150, blank=True)
    profession = models.CharField(choices=PROFESSION,
                                    max_length=20, blank=True,
                                    default="none")
    institution_name = models.CharField(max_length=150, blank=True)
    health_insurance = models.CharField(choices=OPTION,
                                    max_length=20, blank=True,
                                    default="none")
    insurance_details = models.CharField(max_length=200, blank=True)

    profile_image = models.CharField(max_length=900, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
          self.created_date = timezone.now()
          self.save()

    def __unicode__(self):
        return self.first_name + " " + self.last_name

    def __str__(self):
        return self.first_name + " " + self.last_name

    class Meta:
        verbose_name_plural = "User Profile"
