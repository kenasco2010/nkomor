# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
from django.contrib.auth.views import *
import re
from rest_framework import viewsets, serializers
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from rest_framework.permissions import IsAuthenticated, AllowAny

from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render



from user_profile.serializers import UserProfileSerializer
from user_profile.permissions import IsOwner
from user_profile.models import UserProfile
# Create your views here.


# Rest API Endpoints.
class UserProfileViewSet(viewsets.ModelViewSet):
    # permission_classes = (AllowAny,)
    permission_classes = (IsAuthenticated, )
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()

    # The user profile handles both the creation and editing of user profile.
    @list_route(methods=('POST',), url_path="create-profile", permission_classes = (IsAuthenticated, ))
    def create_profile(self, request, *args, **kwargs):
        try:
            user_profile = UserProfile.objects.get(user=request.user)
            user_profile.first_name=request.data.get('first_name')
            user_profile.last_name=request.data.get('last_name')
            user_profile.phone_number=request.data.get('phone_number')
            email

            user_profile.profile_image=request.data.get('profile_image')
            user_profile.save()
            user_profile = self.serializer_class(user_profile)
            return Response({"status_code": status.HTTP_200_OK,
                             "message": "You have updated your profile details",
                             "result": user_profile.data
                             })
        except UserProfile.DoesNotExist:
            create_user = UserProfile.objects.create(
                first_name=request.data.get('first_name'),
                last_name=request.data.get('last_name'),
                phone_number=request.data.get('phone_number'),
                profile_image=request.data.get('profile_image'),
                user=request.user
            )
            user_profile = self.serializer_class(create_user)
            return Response({"status_code": status.HTTP_201_CREATED,
                             "message": "you have successfully created your profile",
                             "result": user_profile.data
                            })

